import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'diceDot.dart';

class DiceCube extends StatefulWidget {
  const DiceCube();
  @override
  _DiceCubeState createState() => _DiceCubeState();
}

class _DiceCubeState extends State<DiceCube> {

  List<Widget> dice = <Widget>[DiceDot()];

  void rollDice(){
    setState(() {
      int num =1;
      var randNum = new Random();
      num = randNum.nextInt(6)+1;
      switch(num){
        case 1 :
          dice = [DiceDot()];
          break;
        case 2 :
          dice = [DiceDot(),SizedBox(width: 30, height:30,), DiceDot()];
          break;
        case 3 :
          dice = [DiceDot(), DiceDot(), DiceDot()];
          break;
        case 4 :
          dice = [DiceDot(), SizedBox(width: 30, height:30,), DiceDot(),
            SizedBox(width: 30, height:30,),SizedBox(width: 30, height:30,),SizedBox(width: 30, height:30,),
            DiceDot(), SizedBox(width: 30, height:30,), DiceDot()];
          break;
        case 5 :
          dice = [DiceDot(), SizedBox(width: 30, height:30,),DiceDot(),
            SizedBox(width: 30, height:30,),DiceDot(), SizedBox(width: 30, height:30,),
            DiceDot(),SizedBox(width: 30, height:30,), DiceDot()];
          break;
        case 6 :
          dice = [DiceDot(), SizedBox(width: 30, height:30,),DiceDot(),
            DiceDot(), SizedBox(width: 30, height:30,), DiceDot(),
            DiceDot(),SizedBox(width: 30, height:30,), DiceDot()];
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      width: 150,
      child: RaisedButton(
        color: Colors.red,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30) ,
          side: BorderSide(color: Colors.white , width: 5)
        ),
        onPressed: rollDice,
        child: Wrap(
          spacing: 5,
          runSpacing: 5,
          alignment: WrapAlignment.center,
          children: dice
        ),
      ),
    );
  }
}