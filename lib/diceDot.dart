import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DiceDot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 30,
      height:30,
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
    );
  }
}
