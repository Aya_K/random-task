import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'diceCube.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        backgroundColor: Colors.red,
        body: Center(
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          Text("Dicee" , style: TextStyle(color: Colors.white , fontSize: 30),),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              DiceCube(),
              SizedBox(height: 300,
                width: 50,),
              DiceCube(),
            ],
          ),
  ],
        ),),
      ),
      //MyHomePage(title: 'Flutter Demo Home Page')

    );
  }
}
